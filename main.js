let ul = document.getElementById("Pokemon");

fetch("https://pokeapi.co/api/v2/pokemon/")
    .then(d => d.json())
    .then(response => {
        console.log(response)
        response.results.forEach((elt) => {
            let li = createNode('li'),
            heading = createNode("h4");
            heading.innerText = elt.name;
            append(li, heading);
            append(ul, li)
            fetch(elt.url).then(d => d.json()).then(response => {
                let imgUrl = response.sprites.front_default;
                let img = createNode('img');
                img.src = imgUrl;
                img.alt = "Pokemon"
                append(li, img);
                append(ul, li)
            })
        })
    })

function createNode(element) {
    return document.createElement(element); // Create the type of element you pass in the parameters
}

function append(parent, el) {
    return parent.appendChild(el); // Append the second parameter(element) to the first one
}

function search() {
    // Declare variables
    let input, filter, ul, li, a, i, txtValue;
    input = document.getElementById('myInput');
    filter = input.value.toUpperCase();
    ul = document.getElementById("Pokemon");
    li = ul.getElementsByTagName('li');
        // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < li.length; i++) {
      a = li[i].getElementsByTagName("h4")[0];
      txtValue = a.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        li[i].style.display = "";
      } else {
        li[i].style.display = "none";
      }
    }
  }